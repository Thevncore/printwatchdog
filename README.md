# PrintWatchdog
PrintWatchdog is a simple print job processor. 

This tool watches the filesystem for newly-present document files under pdf format and prints them out.
## Why?
I have an old shared printer that is connected to a host PC. And I have so much pain sharing the printer "the right way". If the host was a Linux box (sharing `cups` and `samba`), then other Windows boxes won't print to it no matter what . If the host was a Windows box, then Linux boxes won't cooperate either. That is not to mention Macs yet. Apart from the ideal environment in respective print server/shares server's guides, the environment I was working on didn't quite fit (different subnets and traversal policies, different OS versions, no central domain controller, different users, etc.) which explains why I'm facing with such difficulties.
## How does this tool solve the problem?
Considering the case when one wants to send a print job. Essentially, he is sending a representation of the content. This content is then received on the host PC and sent to the printer.

In the case of `cups` or `samba`, the "representation" could be the rendered or description of content, transferred after some authentication steps, depending on the protocol.

So what causes the problem in the first place is the authentication steps itself, because of many different potential incompatible implementations across devices. It is possible to, considering my case, disable the permission for the printer altogether (passwordless login), but this causes problem with samba, and still doesn't solve the unnecessary authentication steps. 

On the conceptual level, this tool implements the core functionality (i.e "fetch and print") of what any shared printer models are expected to behave: If content is available, then print it. The "content" here is any document in pdf format, present in a specific directory on a filesystem. 
## How does this tool work?
In this particular case, this tool watches for changes in the specified directory. If there is a new pdf document, it then proceeds to read, create a print job and send to the printer.

But there are several situation that some specific settings is desired, namely duplex (simplex, portrait, landscape). To solve this problem, one can define multiple watching directories, each having its own settings.

The rest of the problem being how the document is transferred to the watching directories, can be solved more easily and universally via file sharing protocols like FTP, which is OS independent.
