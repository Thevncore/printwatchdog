﻿using PdfiumViewer;
using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Drawing.Printing;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.NetworkInformation;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PrintWatchdog
{
    public partial class Form1 : Form
    {
        internal static Form1 instance = null;

        internal static PrinterSettings simplexSettings = new PrinterSettings() { Duplex = Duplex.Simplex };
        internal static PrinterSettings verticalSettings = new PrinterSettings() { Duplex = Duplex.Vertical };
        internal static PrinterSettings horizontalSettings = new PrinterSettings() { Duplex = Duplex.Horizontal };
        private List<PrintFolder> folders = new List<PrintFolder>();

        private int time = 0;

        public Form1()
        {
            instance = this;
            if (!File.Exists("config.ini"))
            {
                var z = (from string i in PrinterSettings.InstalledPrinters select i).ToList();
                File.WriteAllLines("config.ini", z);
            }
            else
            {
                try
                {
                    var z = File.ReadAllLines("config.ini");
                    PrintFolder.printerName = z[0];
                    folders.Add(new PrintFolder(z[1], z[2], simplexSettings, "1 mặt"));
                    folders.Add(new PrintFolder(z[3], z[4], verticalSettings, "2 mặt dọc"));
                    folders.Add(new PrintFolder(z[5], z[6], horizontalSettings, "2 mặt ngang"));
                }
                catch (Exception e)
                {
                    MessageBox.Show(e.ToString());
                    File.AppendAllText("errors.txt",
            Environment.NewLine +
            Environment.NewLine +
            "----------------------------------------------------\r\n" +
            DateTime.Now.ToString("dd-MM-yyyy, HH:mm:ss") + Environment.NewLine +
            "Config read error!\r\n" +
            e.ToString() + Environment.NewLine +
            "----------------------------------------------------\r\n" +
            Environment.NewLine +
            Environment.NewLine
            );
                }
            }

            InitializeComponent();

            Text += " (OK, đang chạy)";

        }

        private async void timer1_Tick(object sender, EventArgs e)
        {
            time = time + 1;

            if (time >= 500)
            {
                timer1.Stop();
                time = 0;
                bool FTPGood = await CheckFTP();
                bool NETGood = await PingHost("192.168.9.1");

                if (!FTPGood && !NETGood)
                {
                    lblStatus.Show();
                    lblStatus.BackColor = Color.Red;
                    lblStatus.Text = "FTP không chạy" + Environment.NewLine + "Không có mạng luôn";
                }
                else if (FTPGood && !NETGood)
                {
                    lblStatus.Show();
                    lblStatus.BackColor = Color.Red;
                    lblStatus.Text = "FTP chạy" + Environment.NewLine + "Nhưng mà không có mạng!";
                }
                else if (!FTPGood && NETGood)
                {
                    lblStatus.Show();
                    lblStatus.BackColor = Color.Yellow;
                    lblStatus.Text = "Có mạng" + Environment.NewLine + "Nhưng FTP không chạy!";
                }
                else
                {
                    lblStatus.Hide();
                }
                timer1.Start();
            }

            toolStripProgressBar1.Value = time;
        }

        private async static Task<bool> CheckFTP()
        {
            FtpWebRequest requestDir = (FtpWebRequest)WebRequest.Create("ftp://localhost");
            requestDir.Credentials = new NetworkCredential("anonymous", "password@c.c");
            requestDir.Method = WebRequestMethods.Ftp.ListDirectoryDetails;
            requestDir.KeepAlive = false;
            try
            {
                WebResponse response = await requestDir.GetResponseAsync();
                response.Close();
                return true;
            }
            catch (Exception e)
            {
                return false;
            }
        }

        private async static Task<bool> PingHost(string nameOrAddress)
        {
            bool pingable = false;
            Ping pinger = null;
            try
            {
                pinger = new Ping();
                PingReply reply = await pinger.SendPingAsync(nameOrAddress);
                pingable = reply.Status == IPStatus.Success;
            }
            catch (PingException)
            {
                // Discard PingExceptions and return false;
            }
            finally
            {
                if (pinger != null)
                {
                    pinger.Dispose();
                }
            }

            return pingable;
        }

        private void statusStrip1_Resize(object sender, EventArgs e)
        {
            toolStripProgressBar1.Width = statusStrip1.Width - 50;
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            timer1.Start();
        }
    }

    internal class PrintFolder
    {
        public static string printerName;
        private static bool FileAvailable(string path)
        {
            FileStream stream = null;

            try
            {
                using (stream = File.Open(path, FileMode.Open, FileAccess.ReadWrite, FileShare.None))
                    return true;
            }
            catch (IOException)
            {
                return false;
            }
            finally
            {
                if (stream != null)
                    stream.Close();
            }
        }
        public FileSystemWatcher Watcher { get; private set; }
        public PrinterSettings Setting { get; private set; }
        public string PrintedPath { get; private set; }
        public static List<string> IgnoredList { get; } = new List<string>();

        private string Name;

        public static void ClearIgnoredList()
        {
            IgnoredList.Clear();
        }

        public PrintFolder(string path, string printedPath, PrinterSettings settings, string settingName)
        {
            Watcher = new FileSystemWatcher(path, "*.pdf");
            Watcher.Created += file_created;
            Watcher.EnableRaisingEvents = true;
            PrintedPath = printedPath;
            Setting = settings;
            Name = settingName;
        }

        private static void PrintError(Exception e, string error)
        {
            Form1.instance.Invoke((MethodInvoker)delegate
            {
                string errorText = DateTime.Now.ToString("dd-MM-yyyy, HH:mm:ss") + ": " + error + ", " + e.ToString();
                Form1.instance.errorList.Items.Add(errorText);

                File.AppendAllText("errors.txt",
        Environment.NewLine +
        Environment.NewLine +
        "----------------------------------------------------\r\n" +
        DateTime.Now.ToString("dd-MM-yyyy, HH:mm:ss") + Environment.NewLine +
        error + Environment.NewLine +
        e.ToString() + Environment.NewLine +
        "----------------------------------------------------\r\n" +
        Environment.NewLine +
        Environment.NewLine
        );
            });
        }

        private void PrintError(string error)
        {
            string errorText = DateTime.Now.ToString("dd-MM-yyyy, HH:mm:ss") + ": " + error;
            Form1.instance.errorList.Items.Add(errorText);

            File.AppendAllText("errors.txt",
Environment.NewLine +
Environment.NewLine +
"----------------------------------------------------\r\n" +
DateTime.Now.ToString("dd-MM-yyyy, HH:mm:ss") + Environment.NewLine +
error + Environment.NewLine +
"----------------------------------------------------\r\n" +
Environment.NewLine +
Environment.NewLine
);
        }

        private void file_created(object sender, FileSystemEventArgs e)
        {
            Task.Run(async () => await HandlePrintJobFromFileChange(e));
        }

        public async Task HandlePrintJobFromFileChange(FileSystemEventArgs e)
        {
            int copyWaitRetries = 100;
            while (!FileAvailable(e.FullPath) && copyWaitRetries > 0)
            {
                await Task.Delay(1000);
                copyWaitRetries--;
            }

            if (copyWaitRetries <= 0)
            {
                PrintError("Timeout waiting for file to release. The file has been added to ignore list. Please rename to try again.");
                IgnoredList.Add(e.FullPath);
                return;
            }

            using (var document = PdfDocument.Load(e.FullPath))
            {
                using (var printDocument = document.CreatePrintDocument())
                {
                    try
                    {
                        printDocument.PrinterSettings = Setting;
                        printDocument.PrinterSettings.PrinterName = printerName;
                        printDocument.DocumentName = "[Watchdog]" + Path.GetFileName(e.FullPath);
                        printDocument.PrintController = new StandardPrintController();
                        printDocument.Print();
                    }
                    catch (Exception ex)
                    {
                        PrintError(ex, "Printing error");
                    }
                }
            }

            // Printed     
            try
            {
                Form1.instance.printedList.Items.Add(DateTime.Now.ToString("dd-MM-yyyy, HH:mm:ss") + ": " + Path.GetFileName(e.FullPath) + " (" + Name + ")");
                var moveRetries = 1000;
                while (moveRetries > 0)
                {
                    try
                    {
                        File.Move(e.FullPath, Path.Combine(PrintedPath, Path.GetFileName(e.FullPath)));
                    }
                    catch (Exception ex2)
                    {
                        moveRetries--;
                        Thread.Sleep(500);
                        if (moveRetries <= 0)
                            throw ex2;
                    }
                }

            }
            catch (Exception ex)
            {
                try
                {
                    var destinationPath = Path.Combine(PrintedPath, Path.GetFileName(e.FullPath));

                    if (!File.Exists(e.FullPath))
                        return;

                    if (!File.Exists(destinationPath))
                        File.Copy(e.FullPath, destinationPath);

                    IgnoredList.Add(e.FullPath);
                    PrintError(ex, "Error removing file out of processing folder. The file has been added to ignore list. You can manually remove the file.");
                }
                catch (Exception ex1)
                {
                    PrintError(ex1, "Error touching the file in processing folder. The file has been added to ignore list. You can manually remove the file.");
                    IgnoredList.Add(e.FullPath);
                }
            }
        }

        public static void HandlePrintJobFromStream(Stream stream, string filename, PrinterSettings settings)
        {
            using (var document = PdfDocument.Load(stream))
            {   
                using (var printDocument = document.CreatePrintDocument())
                {
                    try
                    {
                        printDocument.PrinterSettings = settings;
                        printDocument.PrinterSettings.PrinterName = printerName;
                        printDocument.DocumentName = "[Watchdog Online] " + filename;
                        printDocument.PrintController = new StandardPrintController();
                        printDocument.Print();
                    }
                    catch (Exception ex)
                    {
                        PrintError(ex, "Printing error");
                    }
                }
            }

            Form1.instance.Invoke((MethodInvoker)delegate
            {
                Form1.instance.printedList.Items.Add(DateTime.Now.ToString("dd-MM-yyyy, HH:mm:ss") + ": " + filename);
            });
        }
    }

}
