﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace PrintWatchdog
{
    internal static class HttpServer
    {
        public static HttpListener listener;
        public static string url = "http://*:8080/";
        public static string pageData =
            "<!DOCTYPE>" +
            "<html>" +
            "  <body>" +
            "    <p>Select file to print: </p>" +
            "    {0}" +
            "    <form method=\"post\" action=\"print\" enctype=\"multipart/form-data\">" +
            "      <input type=\"file\" name=\"content\">" +
            "      <input type=\"submit\" value=\"Shutdown\" {1}>" +
            "    </form>" +
            "  </body>" +
            "</html>";

        private static Dictionary<string, string> IDResponseMap = new Dictionary<string, string>();

        private static Random random = new Random();
        public static string RandomString(int length)
        {
            const string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
            return new string(Enumerable.Repeat(chars, length)
              .Select(s => s[random.Next(s.Length)]).ToArray());
        }


        public static async Task HandleIncomingConnections()
        {
            bool runServer = true;

            // While a user hasn't visited the `shutdown` url, keep on handling requests
            while (runServer)
            {
                HttpListenerContext ctx = await listener.GetContextAsync();

                HttpListenerRequest req = ctx.Request;
                HttpListenerResponse resp = ctx.Response;
                string response = "";

                if (req.HttpMethod == "GET" && req.Url.AbsolutePath == "/")
                {
                    if (req.Cookies["id"] != null && IDResponseMap.ContainsKey(req.Cookies["id"].Value))
                    {
                        response = IDResponseMap[req.Cookies["id"].Value];
                        resp.Cookies.Add(req.Cookies["id"]);
                        resp.Cookies["id"].Expires = DateTime.Now.AddDays(-1);
                    }
                }

                if ((req.HttpMethod == "POST") && (req.Url.AbsolutePath == "/print"))
                {
                    var id = RandomString(48);
                    resp.SetCookie(new Cookie("id", id));

                    string tempFileName = SaveFile(req.ContentEncoding, GetBoundary(req.ContentType), req.InputStream, out string fileName);
                    using (FileStream fs = new FileStream(tempFileName, FileMode.Open))
                    {
                        try
                        {
                            PrintFolder.HandlePrintJobFromStream(fs, fileName, Form1.verticalSettings);
                            IDResponseMap.Add(id, "Successfully received.");
                        }
                        catch (PdfiumViewer.PdfException)
                        {
                            IDResponseMap.Add(id, "Invalid PDF!");
                            response = "Invalid PDF!";
                        }

                        resp.StatusCode = 303;
                        resp.RedirectLocation = "/";
                        resp.Close();
                        continue;
                    }
                }

                string disableSubmit = !runServer ? "disabled" : "";

                byte[] data = Encoding.UTF8.GetBytes(string.Format(pageData, response, disableSubmit));
                resp.ContentType = "text/html";
                resp.ContentEncoding = Encoding.UTF8;
                resp.ContentLength64 = data.LongLength;

                await resp.OutputStream.WriteAsync(data, 0, data.Length);
                resp.Close();
            }
        }

        public static async Task StartStatisticsServer()
        {
            // Create a Http server and start listening for incoming connections
            listener = new HttpListener();
            listener.Prefixes.Add(url);
            listener.Start();
            //Console.WriteLine("Listening for connections on {0}", url);

            // Handle requests
            //Task listenTask = HandleIncomingConnections();
            //listenTask.GetAwaiter().GetResult();
            await HandleIncomingConnections();

            // Close the listener
            listener.Close();
        }

        private static string SaveFile(Encoding enc, string boundary, Stream input, out string fileName)
        {
            byte[] boundaryBytes = enc.GetBytes(boundary);
            int boundaryLen = boundaryBytes.Length;

            string tempFileName = Path.GetTempFileName();

            using (FileStream output = new FileStream(tempFileName, FileMode.Create, FileAccess.Write))
            {
                byte[] buffer = new byte[1024];
                int len = input.Read(buffer, 0, 1024);
                int startPos = -1;

                // Find start boundary
                while (true)
                {
                    if (len == 0)
                    {
                        throw new Exception("Start Boundaray Not Found");
                    }

                    startPos = IndexOf(buffer, len, boundaryBytes);
                    if (startPos >= 0)
                    {
                        break;
                    }
                    else
                    {
                        Array.Copy(buffer, len - boundaryLen, buffer, 0, boundaryLen);
                        len = input.Read(buffer, boundaryLen, 1024 - boundaryLen);
                    }
                }

                // Skip four lines (Boundary, Content-Disposition, Content-Type, and a blank)
                for (int i = 0; i < 4; i++)
                {
                    while (true)
                    {
                        if (len == 0)
                        {
                            throw new Exception("Preamble not Found.");
                        }

                        startPos = Array.IndexOf(buffer, enc.GetBytes("\n")[0], startPos);
                        if (startPos >= 0)
                        {
                            startPos++;
                            break;
                        }
                        else
                        {
                            len = input.Read(buffer, 0, 1024);
                        }
                    }
                }

                string preamble = Encoding.UTF8.GetString(buffer);
                int startIndex = preamble.IndexOf("filename=\"") + 10; // == 100
                int endIndex = preamble.IndexOf("\"", startIndex);
                fileName = preamble.Substring(startIndex, endIndex - startIndex);


                Array.Copy(buffer, startPos, buffer, 0, len - startPos);
                len = len - startPos;

                while (true)
                {
                    int endPos = IndexOf(buffer, len, boundaryBytes);
                    if (endPos >= 0)
                    {
                        if (endPos > 0) output.Write(buffer, 0, endPos - 2);
                        break;
                    }
                    else if (len <= boundaryLen)
                    {
                        throw new Exception("End Boundaray Not Found");
                    }
                    else
                    {
                        output.Write(buffer, 0, len - boundaryLen);
                        Array.Copy(buffer, len - boundaryLen, buffer, 0, boundaryLen);
                        len = input.Read(buffer, boundaryLen, 1024 - boundaryLen) + boundaryLen;
                    }
                }

                return tempFileName;
            }
        }

        private static int IndexOf(byte[] buffer, int len, byte[] boundaryBytes)
        {
            for (int i = 0; i <= len - boundaryBytes.Length; i++)
            {
                bool match = true;
                for (int j = 0; j < boundaryBytes.Length && match; j++)
                {
                    match = buffer[i + j] == boundaryBytes[j];
                }

                if (match)
                {
                    return i;
                }
            }

            return -1;
        }

        private static string GetBoundary(string ctype)
        {
            return "--" + ctype.Split(';')[1].Split('=')[1];
        }

        public static void Main(string[] args)
        {
            // Create a Http server and start listening for incoming connections
            listener = new HttpListener();
            listener.Prefixes.Add(url);
            listener.Start();
            Console.WriteLine("Listening for connections on {0}", url);

            // Handle requests
            Task listenTask = HandleIncomingConnections();
            listenTask.GetAwaiter().GetResult();

            // Close the listener
            listener.Close();
        }
    }
}
